# dpuipsec

### Description

This is an Ansible Playbook for a DPU PoC that creates an IPsec tunnel between two Bluefield-2(+) DPU using VXLAN over the same L2 subnet.

### IMPORTANT

This demo was tested against BFB 3.8.0 with DOCA 1.2.

The DPU IPsec demo requires that the Bluefield-2(+) is setup in Embedded mode. Go to the following demo:

https://gitlab-master.nvidia.com/mcourtney/dpu-poc-kit#running-the-playbooks

and run the following playbook:

```
ansible-playbook poc-embedded-mode.yml
```

### Caveat

This demo WILL NOT work if you are using DHCP on the BF2 interfaces. See step #2 below.

### Instructions

1. First, re-install the BFB on the Bluefield-2(+) and set the card into Embedded mode

2. The IP address for this demo will be on the p0 or p1 interface. Create a 70-ipsec.yaml file under /etc/netplan. In that file, add something similar the below:

```
network:
  version: 2
  ethernets:
    p0:
        addresses:
        - 5.5.5.5/24
```

after the above is in place, run the following:

```
sudo netplan apply
```

3. Run the playbook with the following command:

```
ansible-playbook dpu-poc-bf-to-bf.yml
```

The output will look like the following:

```
mcourtney@ubuntu:~/dpuipsec$ ansible-playbook -l bfipsec1 dpu-poc-bf-to-bf.yml

PLAY [bfs] *****************************************************************************************

TASK [Gathering Facts] *****************************************************************************
Friday 01 October 2021  23:00:33 -0500 (0:00:00.013)       0:00:00.013 ********
ok: [bfipsec1]

TASK [ipsecbf2 : Grep BF PCI device] ***************************************************************
Friday 01 October 2021  23:00:36 -0500 (0:00:02.790)       0:00:02.804 ********
changed: [bfipsec1]

TASK [ipsecbf2 : Grep BF Parent device] ************************************************************
Friday 01 October 2021  23:00:37 -0500 (0:00:01.577)       0:00:04.381 ********
changed: [bfipsec1]

TASK [ipsecbf2 : Enable IPsec on the Bluefield 1/5] ************************************************
Friday 01 October 2021  23:00:39 -0500 (0:00:01.449)       0:00:05.831 ********

TASK [ipsecbf2 : Pause for 5 seconds] **************************************************************
Friday 01 October 2021  23:00:39 -0500 (0:00:00.032)       0:00:05.863 ********
Pausing for 5 seconds
(ctrl+C then 'C' = continue early, ctrl+C then 'A' = abort)
ok: [bfipsec1]

TASK [ipsecbf2 : Enable IPsec on the Bluefield 2/5] ************************************************
Friday 01 October 2021  23:00:44 -0500 (0:00:05.014)       0:00:10.877 ********
changed: [bfipsec1] => (item=devlink dev eswitch set pci/0000:03:00.0 mode legacy)

TASK [ipsecbf2 : Pause for 5 seconds] **************************************************************
Friday 01 October 2021  23:00:47 -0500 (0:00:02.835)       0:00:13.712 ********
Pausing for 5 seconds
(ctrl+C then 'C' = continue early, ctrl+C then 'A' = abort)
ok: [bfipsec1]

TASK [ipsecbf2 : Enable IPsec on the Bluefield 3/5] ************************************************
Friday 01 October 2021  23:00:52 -0500 (0:00:05.013)       0:00:18.726 ********
changed: [bfipsec1] => (item=echo full | sudo tee -a '/sys/class/net/p0/compat/devlink/ipsec_mode')
changed: [bfipsec1] => (item=echo dmfs | sudo tee -a '/sys/bus/pci/devices/0000:03:00.0/net/p0/compat/devlink/steering_mode')

TASK [ipsecbf2 : Enable IPsec on the Bluefield 4/5] ************************************************
Friday 01 October 2021  23:00:53 -0500 (0:00:01.679)       0:00:20.405 ********
changed: [bfipsec1] => (item=devlink dev eswitch set pci/0000:03:00.0 mode switchdev)

TASK [ipsecbf2 : Pause for 5 seconds] **************************************************************
Friday 01 October 2021  23:00:57 -0500 (0:00:03.337)       0:00:23.743 ********
Pausing for 5 seconds
(ctrl+C then 'C' = continue early, ctrl+C then 'A' = abort)
ok: [bfipsec1]

TASK [ipsecbf2 : Enable IPsec on the Bluefield 5/5] ************************************************
Friday 01 October 2021  23:01:02 -0500 (0:00:05.019)       0:00:28.762 ********
changed: [bfipsec1] => (item=ethtool -K p0 hw-tc-offload on)

TASK [ipsecbf2 : Install StrongSwan configuration files] *******************************************
Friday 01 October 2021  23:01:03 -0500 (0:00:01.350)       0:00:30.112 ********
changed: [bfipsec1]

TASK [ipsecbf2 : Load the IPSec Tunnel] ************************************************************
Friday 01 October 2021  23:01:05 -0500 (0:00:01.898)       0:00:32.011 ********
changed: [bfipsec1]

RUNNING HANDLER [ipsecbf2 : restart strongswan] ****************************************************
Friday 01 October 2021  23:01:06 -0500 (0:00:01.381)       0:00:33.393 ********
changed: [bfipsec1]

PLAY RECAP *****************************************************************************************
bfipsec1                   : ok=13   changed=9    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

Friday 01 October 2021  23:01:10 -0500 (0:00:03.712)       0:00:37.105 ********
===============================================================================
ipsecbf2 : Pause for 5 seconds -------------------------------------------------------------- 5.02s
ipsecbf2 : Pause for 5 seconds -------------------------------------------------------------- 5.01s
ipsecbf2 : Pause for 5 seconds -------------------------------------------------------------- 5.01s
ipsecbf2 : restart strongswan --------------------------------------------------------------- 3.71s
ipsecbf2 : Enable IPsec on the Bluefield 4/5 ------------------------------------------------ 3.34s
ipsecbf2 : Enable IPsec on the Bluefield 2/5 ------------------------------------------------ 2.84s
Gathering Facts ----------------------------------------------------------------------------- 2.79s
ipsecbf2 : Install StrongSwan configuration files ------------------------------------------- 1.90s
ipsecbf2 : Enable IPsec on the Bluefield 3/5 ------------------------------------------------ 1.68s
ipsecbf2 : Grep BF PCI device --------------------------------------------------------------- 1.58s
ipsecbf2 : Grep BF Parent device ------------------------------------------------------------ 1.45s
ipsecbf2 : Load the IPSec Tunnel ------------------------------------------------------------ 1.38s
ipsecbf2 : Enable IPsec on the Bluefield 5/5 ------------------------------------------------ 1.35s
ipsecbf2 : Enable IPsec on the Bluefield 1/5 ------------------------------------------------ 0.03s
```

4. On one of the Bluefield-2(+)'s, run the following command to establish the IPsec tunnel:

```
sudo swanctl --load-all
sudo swanctl -i --child bf
```

Output:

```
[IKE] initiating IKE_SA BFL-BFR[4] to 5.5.5.100
[ENC] generating IKE_SA_INIT request 0 [ SA KE No N(NATD_S_IP) N(NATD_D_IP) N(FRAG_SUP) N(HASH_ALG) N(REDIR_SUP) ]
[NET] sending packet: from 5.5.5.5[500] to 5.5.5.100[500] (240 bytes)
[NET] received packet: from 5.5.5.100[500] to 5.5.5.5[500] (248 bytes)
[ENC] parsed IKE_SA_INIT response 0 [ SA KE No N(NATD_S_IP) N(NATD_D_IP) N(FRAG_SUP) N(HASH_ALG) N(CHDLESS_SUP) N(MULT_AUTH) ]
[CFG] selected proposal: IKE:AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
[IKE] authentication of 'host1' (myself) with pre-shared key
[IKE] establishing CHILD_SA bf{4}
[ENC] generating IKE_AUTH request 1 [ IDi N(INIT_CONTACT) IDr AUTH N(USE_TRANSP) SA TSi TSr N(MULT_AUTH) N(EAP_ONLY) N(MSG_ID_SYN_SUP) ]
[NET] sending packet: from 5.5.5.5[500] to 5.5.5.100[500] (256 bytes)
[NET] received packet: from 5.5.5.100[500] to 5.5.5.5[500] (224 bytes)
[ENC] parsed IKE_AUTH response 1 [ IDr AUTH N(USE_TRANSP) SA TSi TSr ]
[IKE] authentication of 'host2' with pre-shared key successful
[IKE] IKE_SA BFL-BFR[4] established between 5.5.5.5[host1]...5.5.5.100[host2]
[CFG] selected proposal: ESP:AES_GCM_16_128/NO_EXT_SEQ
[IKE] CHILD_SA bf{4} established with SPIs c476e054_i cd13e418_o and TS 5.5.5.5/32[udp/4789] === 5.5.5.100/32[udp/4789]
initiate completed successfully
```

### Troubleshooting StrongSwan

Helpful commands:

```
sudo ipsec status
sudo swanctl -l
sudo swanctl --log
sudo ipsec statusall
sudo swanctl -t --ike BFL-BFR
```

### Troubleshooting the DPU

1. The DPUs are currently limited in terms of troubleshooting. If you're having an issue, it is best to start from scratch and re-run the Install / PoC Kit:

https://confluence.nvidia.com/display/NetworkingBU/SA+DPU+PoC+Kit

There is a "reinstall" playbook which reflashes the BFB without the need to re-run the entire PoC Kit:

```
ansible-playbook reinstall-bfb.yml
```
